from textblob import TextBlob
import re
import json
import os

#lecture
def extract_from_json():
    with open("outputs/tweets_backup.json") as f:
        my_dictionnary = json.load(f)
    return my_dictionnary

data = extract_from_json()

def clean_tweet(tweet):
    return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", tweet).split())


def analyze_sentiment(tweet):
    analysis = TextBlob(clean_tweet(tweet))

    if analysis.sentiment.polarity > 0:
        return 1
    elif analysis.sentiment.polarity == 0:
        return 0
    else:
        return -1

def save_to_json(my_dict, path="outputs", filename="tweets_backup.json"):
    """
    Sauvegarde le dictionnaire passé en argument au format json
    :param my_dict: Dictionnaire à sauvegarder
    :type my_dict: Dict
    :param path: Path for the json file
    :type path: Str
    :param filename: The name of the json file
    :type filename: Str

    :return: N/A
    """
    if not os.path.exists(path):
        os.makedirs(path)
    with open(path+"/"+filename, "w") as f:
        json.dump(my_dict, f)


def extract_from_json(path="outputs", filename="tweets_backup.json"):
    """
    Exporte vers un dictionnaire les données contenues dans le fichier json spécifié.
    :param path: Path for the json file
    :type path: Str
    :param filename: The name of the json file
    :type filename: Str

    :return: The dictionnary containing the data of the json file
    :rtype: Dict
    """
    with open(path+"/"+filename, "r") as f:
        my_dictionnary = json.load(f)
    return my_dictionnary

name_list = ['Australia','Canada','India','Ireland','New Zealand','Singapore']
name_list2 = ['United Kingdom','UK']
name_list3 = ['United States','US']


def analyze_all_sentiments():
    for name in name_list :
        positif=0
        neutre=0
        negatif=0
        AVpositif = 0
        AVneutre = 0
        AVnegatif = 0
        for id in data:
            try :
                if name in data[id]["query"] :
                    x=analyze_sentiment(data[id]['text'])
                    if x == 1:
                        positif+=1
                    if x == 0:
                        neutre+=1
                    if x == -1:
                        negatif+=1
                    if "vax" in data[id]["query"] :
                        if x == 1:
                            AVpositif+=1
                        if x == 0:
                            AVneutre+=1
                        if x == -1:
                            AVnegatif+=1
            except :
                pass
        data_map = extract_from_json(filename="map.json")
        if positif+negatif+neutre != 0 :
            data_map[name]["vaccine positivity"] = positif * 100 / (positif+negatif+neutre)
        if AVpositif + AVnegatif + AVneutre != 0 :
            data_map[name]["antivax positivity"] = AVpositif * 100 / (AVpositif + AVnegatif + AVneutre)
        save_to_json(data_map,filename="map.json")


    positif=0
    neutre=0
    negatif=0
    AVpositif = 0
    AVneutre = 0
    AVnegatif = 0
    for name in name_list2 :
        for id in data:
            try :
                if name in data[id]["query"] :
                    x=analyze_sentiment(data[id]['text'])
                    if x == 1:
                        positif+=1
                    if x == 0:
                        neutre+=1
                    if x == -1:
                        negatif+=1
                    if "vax" in data[id]["query"] :
                        if x == 1:
                            AVpositif+=1
                        if x == 0:
                            AVneutre+=1
                        if x == -1:
                            AVnegatif+=1
            except :
                pass
    data_map = extract_from_json(filename="map.json")
    if positif+negatif+neutre != 0 :
        data_map["UK"]["vaccine positivity"] = positif * 100 / (positif+negatif+neutre)
    if AVpositif + AVnegatif + AVneutre != 0 :
        data_map["UK"]["antivax positivity"] = AVpositif * 100 / (AVpositif + AVnegatif + AVneutre)
    save_to_json(data_map,filename="map.json")


    positif=0
    neutre=0
    negatif=0
    AVpositif = 0
    AVneutre = 0
    AVnegatif = 0
    for name in name_list3 :
        for id in data:
            try :
                if name in data[id]["query"] :
                    x=analyze_sentiment(data[id]['text'])
                    if x == 1:
                        positif+=1
                    if x == 0:
                        neutre+=1
                    if x == -1:
                        negatif+=1
                    if "vax" in data[id]["query"] :
                        if x == 1:
                            AVpositif+=1
                        if x == 0:
                            AVneutre+=1
                        if x == -1:
                            AVnegatif+=1
            except :
                pass
    data_map = extract_from_json(filename="map.json")
    if positif+negatif+neutre != 0 :
        data_map["US"]["vaccine positivity"] = positif * 100 / (positif+negatif+neutre)
    if AVpositif + AVnegatif + AVneutre != 0 :
        data_map["US"]["antivax positivity"] = AVpositif * 100 / (AVpositif + AVnegatif + AVneutre)
    save_to_json(data_map,filename="map.json")



#analyze_all_sentiments()


