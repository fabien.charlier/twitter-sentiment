Go, Alec, Richa Bhayani, and Lei Huang. "Twitter sentiment classification using distant supervision." CS224N project report, Stanford 1.12 (2009): 2009.

Format :
Qualité (0 : négatif, 2 : neutre, 4 : positif), ID, Date, Requête à l'origine, user_ID, texte

Plus d'info ici : http://help.sentiment140.com/for-students