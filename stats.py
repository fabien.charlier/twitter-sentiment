import json
import requests


# Lecture

with open("datasets/SS-Twitter/pp.json", "r") as read_file:
    data = json.load(read_file)


# Counts

positif = 0
neutre = 0
negatif = 0

for twit in data:
    if twit["labels"] == -1:
        negatif += 1
    if twit["labels"] == 0:
        neutre += 1
    if twit["labels"] == -1:
        positif += 1

print("positif : ", positif, "| negatif :", negatif, "| neutre : ", neutre)

moyenne = (positif - negatif)/2

# comptage de mots

split_words = set()

for twit in data:
    split_words = split_words | set(twit["text"].split())
print("Number of words in text: ", len(split_words))
print(split_words)
