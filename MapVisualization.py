import folium as fo
import webbrowser
import json

def extract_from_json(path="outputs", filename="tweets_backup.json"):
    """
    Exporte vers un dictionnaire les données contenues dans le fichier json spécifié.
    :param path: Path for the json file
    :type path: Str
    :param filename: The name of the json file
    :type filename: Str

    :return: The dictionnary containing the data of the json file
    :rtype: Dict
    """
    with open(path+"/"+filename, "r") as f:
        my_dictionnary = json.load(f)
    return my_dictionnary

name_dict = extract_from_json(path="outputs",filename="map.json")


def create_map():
    m = fo.Map(location = [48.8534 , 2.3488], zoom_start = 2)
    for key,value in name_dict.items() :
        fo.Marker([value["locationx"],value["locationy"]], popup=fo.Popup(key+"<br>Antivax popularity : {:.2f} %<br>Covid19 vax popularity : {:.2f} %".format(value["antivax positivity"],value["vaccine positivity"]),max_width=200), tooltip=key).add_to(m)
    m.save("map.html")
    webbrowser.open("map.html")

create_map()