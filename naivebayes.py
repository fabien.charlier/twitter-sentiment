import json
import sklearn.feature_extraction.text as skvec
import pandas as pd
from nltk.tokenize import word_tokenize

def extract_from_json(path="outputs", filename="tweets_backup.json"):
    """
    Exporte vers un dictionnaire les données contenues dans le fichier json spécifié.
    :param path: Path for the json file
    :type path: Str
    :param filename: The name of the json file
    :type filename: Str

    :return: The dictionnary containing the data of the json file
    :rtype: Dict
    """
    with open(path+"/"+filename, "r") as f:
        my_dictionnary = json.load(f)
    return my_dictionnary

def generate_typical_data(path_list=["datasets/semeval","datasets/SS-Twitter","datasets/STS-Test"], filename_list=["group5_semeval_preprocessed.json","group5_ss_preprocessed.json","group5_sts_preprocessed.json"]):
    data = []
    for i in range(3) :
        path = path_list[i]
        filename = filename_list[i]
        new_data = extract_from_json(path,filename)
        data = data + new_data
    Xtrain0 = []
    Xtrain1 = []
    Xtrain2 = []
    Xtraintot = []
    Xtest = []
    Ytest = []
    i=0
    limit = 9.5 * len(data) / 10
    for element in data:
        if i < limit :
            if element["labels"] == 0 :
                Xtrain0.append(element["text"])
            if element["labels"] == 1 :
                Xtrain1.append(element["text"])
            if element["labels"] == -1 :
                Xtrain2.append(element["text"])
            Xtraintot.append(element["text"])
        else :
            Xtest.append(element["text"])
            Ytest.append(element["labels"])
        i += 1

    return Xtrain0,Xtrain1,Xtrain2,Xtraintot,Xtest,Ytest


def create_dicts(Xtrain0,Xtrain1,Xtrain2,Xtraintot):
    vec0 = skvec.CountVectorizer()
    vec1 = skvec.CountVectorizer()
    vec2 = skvec.CountVectorizer()
    vectot = skvec.CountVectorizer()
    X0 = vec0.fit_transform(Xtrain0)
    X1 = vec1.fit_transform(Xtrain1)
    X2 = vec2.fit_transform(Xtrain2)
    Xtot = vectot.fit_transform(Xtraintot)


    word_list0 = vec0.get_feature_names()
    word_list1 = vec1.get_feature_names()
    word_list2 = vec2.get_feature_names()
    count_list0 = X0.toarray().sum(axis=0)
    count_list1 = X1.toarray().sum(axis=0)
    count_list2 = X2.toarray().sum(axis=0)
    freq0 = dict(zip(word_list0,count_list0))
    freq1 = dict(zip(word_list1,count_list1))
    freq2 = dict(zip(word_list2,count_list2))

    total_cnts_features0 = count_list0.sum(axis=0)
    total_cnts_features1 = count_list1.sum(axis=0)
    total_cnts_features2 = count_list2.sum(axis=0)
    total_features = len(vectot.get_feature_names())


    return freq0,freq1,freq2,total_cnts_features0,total_cnts_features1,total_cnts_features2,total_features

def perform_naive_bayes(new_sentence,freq0,freq1,freq2,total_cnts_features0,total_cnts_features1,total_cnts_features2,total_features):
    new_word_list = word_tokenize(new_sentence)
    prob0_with_ls = 1
    for word in new_word_list:
        if word in freq0.keys():
            count = freq0[word]
        else:
            count = 0
        prob0_with_ls = prob0_with_ls * (count + 1)/(total_cnts_features0 + total_features)
    prob0_with_ls = prob0_with_ls * (total_cnts_features0/total_features)
    prob1_with_ls = 1
    for word in new_word_list:
        if word in freq1.keys():
            count = freq1[word]
        else:
            count = 0
        prob1_with_ls = prob1_with_ls * (count + 1)/(total_cnts_features1 + total_features)
    prob1_with_ls = prob1_with_ls * (total_cnts_features1/total_features)
    prob2_with_ls = 1
    for word in new_word_list:
        if word in freq2.keys():
            count = freq2[word]
        else:
            count = 0
        prob2_with_ls = prob2_with_ls * (count + 1)/(total_cnts_features2 + total_features)
    prob2_with_ls = prob2_with_ls * (total_cnts_features2/total_features)


    if prob0_with_ls > prob1_with_ls and prob0_with_ls > prob2_with_ls :
        predicted_class = 0
    elif prob1_with_ls > prob0_with_ls and prob1_with_ls > prob2_with_ls :
        predicted_class = 1
    elif prob2_with_ls > prob0_with_ls and prob2_with_ls > prob1_with_ls :
        predicted_class = -1
    else :
        print("Houston, we have a problem : no probability is greater than the two others")
    return predicted_class


def test_naive_bayes():
    right = 0
    false = 0
    Xtrain0,Xtrain1,Xtrain2,Xtraintot,Xtest,Ytest = generate_typical_data()
    freq0,freq1,freq2,prob_dict0,prob_dict1,prob_dict2,total_cnts_features0,total_cnts_features1,total_cnts_features2,total_features = create_dicts(Xtrain0,Xtrain1,Xtrain2,Xtraintot)
    for i in range(len(Xtest)):
        predicted_class = perform_naive_bayes(Xtest[i],freq0,freq1,freq2,total_cnts_features0,total_cnts_features1,total_cnts_features2,total_features)
        true_class = Ytest[i]
        if predicted_class == true_class :
            right += 1
        else :
            false += 1
    print("{} out of {} predictions are right".format(right,right+false))

test_naive_bayes()